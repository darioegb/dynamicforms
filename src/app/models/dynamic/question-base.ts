import { EventType } from "./event-type";
export class QuestionBase<T> {
  value: T;
  id: number;
  parentId: number;
  key: string;
  label: string;
  info: string;
  required: boolean;
  visible: boolean;
  order: number;
  controlType: string;
  disabled: boolean;
  childrens: Array<number>;
  events?: Array<EventType>;
  hasCurrentValue = false;

  constructor(options: {
      value?: T,
      id?: number,
      parentId?: number,
      key?: string,
      label?: string,
      info?: string,
      required?: boolean,
      visible?: boolean,
      order?: number,
      controlType?: string,
      disabled?: boolean,
      childrens?: Array<number>,
      events?: Array<EventType>
    } = {}) {
    this.value = options.value;
    this.id = options.id;
    this.parentId = options.parentId || null;
    this.key = options.key || '';
    this.label = options.label || '';
    this.info = options.info || '';
    this.required = !!options.required;
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || '';
    this.disabled = options.disabled || null;
    this.visible = options.visible || false;
    this.events = options.events || null;
    this.childrens = options.childrens || [];
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/