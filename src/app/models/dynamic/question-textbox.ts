import { QuestionBase } from './question-base';
import { EventType } from './event-type';

export class TextboxQuestion extends QuestionBase<string> {
  controlType = 'textbox';
  type: string;
  placeholder: string;
  pattern: string;
  minLength: number;
  maxLength: number;

  constructor(options: {
      value?: any,
      id?: number,
      parentId?: number,
      key?: string,
      label?: string,
      info?: string,
      required?: boolean,
      visible?: boolean,
      order?: number,
      controlType?: string,
      disabled?: boolean,
      childrens?: Array<number>,
      events?: Array<EventType>,
      type?: string,
      placeholder?: string,
      pattern?: string,
      minLength?: number,
      maxLength?: number} = {}) {
    super(options);
    this.type = options.type || '';
    this.placeholder = options.placeholder || '';
    this.pattern = options.pattern || '';
    this.minLength = options.minLength || null;
    this.maxLength = options.maxLength || null;
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/