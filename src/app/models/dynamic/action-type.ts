export class ActionType {
    condition?: string;
    range?: Array<number>;
    set?: Array<any>;
    object?: any;
    constructor(
        public next: number,
        condition?: string,
        object?: any,
        range?: Array<number>,
        set?: Array<any>,
        ) {
        this.condition = condition || null;
        this.object = object || null;
        this.range = range || null;
        this.set = set || null;
    }
}
