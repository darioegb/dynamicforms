import { QuestionBase } from './question-base';
import { EventType } from './event-type';

export class CheckboxQuestion extends QuestionBase<string> {
  controlType = 'checkbox';
  defaultChecked: boolean;
  defaultValue: boolean;
  checked: boolean;
  indeterminate: boolean;

  constructor(options: {
      value?: any,
      id?: number,
      parentId?: number,
      key?: string,
      label?: string,
      info?: string,
      required?: boolean,
      order?: number,
      controlType?: string,
      disabled?: boolean,
      childrens?: Array<number>,
      events?: Array<EventType>,
      defaultChecked?: boolean,
      defaultValue?: boolean,
      checked?: boolean,
      indeterminate?: boolean} = {}) {
    super(options);
    this.defaultChecked = options.defaultChecked || false;
    this.defaultValue = options.defaultValue || false;
    this.checked = options.checked || false;
    this.indeterminate = options.indeterminate || false;
  }
}