import { QuestionBase } from './question-base';

export class HtmlQuestion extends QuestionBase<string> {
  controlType = 'html';
  content: string;

  constructor(options: {} = {}) {
    super(options);
    this.content = options['content'] || '';
  }
}
