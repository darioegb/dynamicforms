import { QuestionBase } from './question-base';
import { EventType } from './event-type';

export class DateQuestion extends QuestionBase<string> {
  controlType = 'datebox';
  type: string;
  min: string;
  max: string;
  autocomplete: boolean;
  step: number;

  constructor(options: {
      value?: any,
      id?: number,
      parentId?: number,
      key?: string,
      label?: string,
      info?: string,
      required?: boolean,
      order?: number,
      controlType?: string,
      disabled?: boolean,
      childrens?: Array<number>,
      events?: Array<EventType>,
      type?: string,
      placeholder?: string,
      min?: string,
      max?: string,
      autocomplete?: boolean,
      step?: number }) {
    super(options);
    this.type = options.type || '';
    this.min = options.min || null;
    this.max = options.max || null;
    this.autocomplete = options.autocomplete || false;
    this.step = options.step || null;
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/