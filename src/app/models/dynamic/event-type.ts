import { ActionType } from "./action-type";

export class EventType {
    name: string;
    actions: Array<ActionType>;

    constructor(options: {
        name?: string,
        actions?: Array<ActionType>} = {}) {
        this.name = options.name || '';
        this.actions = options.actions || null;
    }
}