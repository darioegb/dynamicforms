import { Injectable } from '@angular/core';

import { QuestionBase, CheckboxQuestion, DropdownQuestion, SearchBoxQuestion, TextboxQuestion, DateQuestion, RadioButtonQuestion, NumberQuestion, HtmlQuestion } from '../models/dynamic';


@Injectable()
export class QuestionService {

  // TODO: get from a remote source of question metadata
  // TODO: make asynchronous
  getQuestions() {
    let questions: QuestionBase<any>[] = [
      new HtmlQuestion({
        id: 1,
        key: 'html',
        visible: true,
        order: 0,
        content: "<div class=\"pb-2\"><div class=\"card\"><div class=\"card-body\"><img class=\"img-fluid float-right\" src=\"./assets/images/logo.png\"></img><h4>Formulario Encuesta Provincial</h4><h4>\"Caracterización del turista que visita Mendoza\"</h4></div></div></div>"
      }), 
      new RadioButtonQuestion({
        id: 1,
        key: 'radioButtonSexo',
        label: 'Sexo',
        required: true,
        options: [
          { key: 'male', value: 'Varón' },
          { key: 'female', value: 'Mujer' }
        ],
        visible: true,
        order: 1,
        events: [
          {
            name: 'change',
            actions: [
              {
                next: 2,
                condition: '=',
                object: 'female'
              }
            ]
          }
        ],
        childrens: [3, 4, 7]
      }),
      new NumberQuestion({
        id: 3,
        parentId: 1,
        key: 'numberEdad',
        label: '¿Cuántos años tiene usted?',
        required: true,
        order: 3,
        min: 15,
        max: 110
      }),
      new DropdownQuestion({
        id: 4,
        parentId: 1,
        key: 'dropdownPais',
        label: '¿Cuál es su lugar de residencia habitual?',
        options: [
          { label: 'Seleccione una opcion', value: '', disabled: true },
          { label: 'Argentina', value: 'argentina' },
          { label: 'Chile', value: 'chile' },
          { label: 'Brasil', value: 'brasil' },
          { label: 'Perú', value: 'peru' }
        ],
        bindValue: 'value',
        bindLabel: 'label',
        searchable: true,
        clearSearchOnAdd: true,
        closeOnSelect: true,
        required: true,
        order: 4,
        events: [
          {
            name: 'change',
            actions: [
              {
                next: 5,
                condition: '=',
                object: 'argentina'
              },
              {
                next: 6,
                set: ['chile', 'brasil', 'peru']
              }
            ]
          }
        ]
      }),
      new DropdownQuestion({
        id: 5,
        key: 'dropdownProvincia',
        label: '¿En qué provincia?',
        options: [
          { label: 'Seleccione una opción', value: '', disabled: true },
          { label: 'Capital Federal', value: 'capital' },
          { label: 'Mendoza', value: 'mendoza' },
          { label: 'Córdoba', value: 'cordoba' },
          { label: 'Santa Fé', value: 'santafe' }
        ],
        bindValue: 'value',
        bindLabel: 'label',
        searchable: true,
        clearSearchOnAdd: true,
        closeOnSelect: true,
        required: true,
        order: 5
      }),
      new NumberQuestion({
        id: 6,
        key: 'numberDiasProvincia',
        label: '¿Cuántos días tiene pensado permanecer en la Provincia?',
        required: true,
        order: 6,
        min: 1,
        max: 360
      }),
      new NumberQuestion({
        id: 2,
        key: 'numberPeluqueria',
        label: '¿Cuántas veces va a la peluquería al mes?',
        required: true,
        order: 2,
        min: 0,
        max: 6
      }),
      new DropdownQuestion({
        id: 7,
        key: 'dropdownAccompained',
        label: '¿Con quién viaja?',
        options: [
          { label: 'Seleccione una opción', value: '', disabled: true },
          { label: 'Solo', value: 'solo' },
          { label: 'Familiares', value: 'familiares' },
          { label: 'Con Amigos', value: 'gamigos' },
          { label: 'Grupo de estudiantes', value: 'gestudiantes' },
          { label: 'Grupo de Trabajo', value: 'gtrabajo' },
          { label: 'Gtupo de Jubilados', value: 'gjubilados' },
          { label: 'Otros', value: 'otros' }
        ],
        multiple: true,
        bindValue: 'value',
        bindLabel: 'label',
        searchable: true,
        clearSearchOnAdd: true,
        closeOnSelect: true,
        required: true,
        childrens: [9],
        order: 7,
        events: [
          {
            name: 'change',
            actions: [
              {
                next: 8,
                condition: '=',
                object: 'otros'
              }
            ]
          }
        ],
      }),
      new TextboxQuestion({
        id: 8,
        key: '8_especificar',
        label: 'Especificar',
        placeholder: 'Ingrese detalle',
        required: true,
        order: 8,
        minLength: 1,
        maxLength: 10
      }),
      new NumberQuestion({
        id: 9,
        key: 'numberHowMany',
        label: '¿Cuántos son?',
        required: true,
        order: 9,
        childrens: [10],
        min: 1,
        max: 30
      }),
      new DropdownQuestion({
        id: 10,
        key: 'dropdownOrganizer',
        label: '¿Quién organizó el viaje?',
        options: [
          { label: 'Seleccione una opción', value: '', disabled: true },
          { label: 'Usted. Mismo', value: 'usted' },
          { label: 'Por una agencia de viajes', value: 'oagviajes' },
          { label: 'Por una institución u Obra Social', value: 'oosocial' },
          { label: 'Por su lugar de trabajo', value: 'otrabajo' },
          { label: 'Otros', value: 'otros' }
        ],
        multiple: true,
        bindValue: 'value',
        bindLabel: 'label',
        searchable: true,
        clearSearchOnAdd: true,
        closeOnSelect: true,
        required: true,
        order: 10,
        childrens: [12],
        events: [
          {
            name: 'change',
            actions: [
              {
                next: 11,
                condition: '=',
                object: 'otros'
              }
            ]
          }
        ],
      }),
      new TextboxQuestion({
        id: 11,
        key: '11_Especificar',
        label: 'Especificar',
        placeholder: 'Ingrese detalle',
        required: true,
        order: 11,
        minLength: 1,
        maxLength: 100
      }),

      new DropdownQuestion({
        id: 12,
        key: 'dropdownLocations1',
        label: '¿Para usted cúales son los tres principales atractivos de la Provincia?',
        info: 'Asignar categoría 1, 2 o 3 según la importancia que dé el turista',
        options: [
          { label: 'Seleccione una opción', value: '', disabled: true },
          { label: 'La cordillera', value: 'cordillera' },
          { label: 'Vinos y Bodegas', value: 'vinosybodegas' },
          { label: 'Ciudad de Mendoza', value: 'mendoza' },
          { label: 'Clima', value: 'otrabaclima' },
          { label: 'Otros', value: 'otros' }
        ],
        placeholder: 'Atractivo nro 1',
        bindValue: 'value',
        bindLabel: 'label',
        searchable: true,
        clearSearchOnAdd: true,
        closeOnSelect: true,
        required: true,
        order: 12,
        childrens: [14, 15],
        events: [
          {
            name: 'change',
            actions: [
              {
                next: 13,
                condition: '=',
                object: 'otros'
              }
            ]
          }
        ]
      }),
      new TextboxQuestion({
        id: 13,
        key: '13_Especificar',
        label: 'Especificar',
        placeholder: 'Ingrese detalle atractivo 1',
        required: true,
        order: 13,
        minLength: 1,
        maxLength: 100
      }),
      new DropdownQuestion({
        id: 14,
        key: 'dropdownLocations2',
        options: [
          { label: 'Seleccione una opción', value: '', disabled: true },
          { label: 'La cordillera', value: 'cordillera' },
          { label: 'Vinos y Bodegas', value: 'vinosybodegas' },
          { label: 'Ciudad de Mendoza', value: 'mendoza' },
          { label: 'Clima', value: 'otrabaclima' },
          { label: 'Otros', value: 'otros' }
        ],
        placeholder: 'Atractivo nro 2',
        bindValue: 'value',
        bindLabel: 'label',
        searchable: true,
        clearSearchOnAdd: true,
        closeOnSelect: true,
        required: true,
        order: 14,
        events: [
          {
            name: 'change',
            actions: [
              {
                next: 16,
                condition: '=',
                object: 'otros'
              }
            ]
          }
        ]
      }),
      new DropdownQuestion({
        id: 15,
        key: 'dropdownLocations3',
        options: [
          { label: 'Seleccione una opción', value: '', disabled: true },
          { label: 'La cordillera', value: 'cordillera' },
          { label: 'Vinos y Bodegas', value: 'vinosybodegas' },
          { label: 'Ciudad de Mendoza', value: 'mendoza' },
          { label: 'Clima', value: 'otrabaclima' },
          { label: 'Otros', value: 'otros' }
        ],
        placeholder: 'Atractivo nro 3',
        bindValue: 'value',
        bindLabel: 'label',
        searchable: true,
        clearSearchOnAdd: true,
        closeOnSelect: true,
        required: true,
        order: 16,
        events: [
          {
            name: 'change',
            actions: [
              {
                next: 17,
                condition: '=',
                object: 'otros'
              }
            ]
          }
        ]
      }),
      new TextboxQuestion({
        id: 16,
        key: '16_Especificar',
        label: 'Especificar',
        placeholder: 'Ingrese detalle atractivo 2',
        required: true,
        order: 15,
        minLength: 1,
        maxLength: 100
      }),
      new TextboxQuestion({
        id: 17,
        key: '17_Especificar',
        label: 'Especificar',
        placeholder: 'Ingrese detalle atractivo 3',
        required: true,
        order: 17,
        minLength: 1,
        maxLength: 100
      }),
      /*
   
    
         new DropdownQuestion({
           key: 'brave1',
           label: 'Bravery Rating',
           options: [
             {label: 'Seleccione una opcion', value: '', disabled: true},
             {label: 'Solid', value: 'solid'},
             {label: 'Great', value: 'great'},
             {label: 'Good', value: 'good'},
             {label: 'Unproven', value: 'unproven'}
           ],
           value: ['solid','great'],
           bindValue: 'value',
           bindLabel: 'label',
           searchable: true,
           clearSearchOnAdd: true,
           closeOnSelect: true,
           multiple: true,
           required: true,
           visible: true,
           order: 1,
           events: [
             {
               name: 'change',
               actions: [
                 {
                   next: 5,
                   condition: '!=',
                   object: 'solid'
                 }
               ]
             }
           ]
         }),
   
         new DropdownQuestion({
           key: 'brave2',
           label: 'Bravery Rating',
           options: [
             {label: 'Seleccione una opcion', value: '', disabled: true},
             {label: 'Solid', value: 'solid'},
             {label: 'Great', value: 'great'},
             {label: 'Good', value: 'good'},
             {label: 'Unproven', value: 'unproven'}
           ],
           value: 'good',
           bindValue: 'value',
           bindLabel: 'label',
           searchable: true,
           clearSearchOnAdd: true,
           closeOnSelect: true,
           required: true,
           order: 2
         }),
   
         new SearchBoxQuestion({
           key: 'brave3',
           label: 'Bravery Rating 3',
           options: [
             {label: 'buscar', value: ''},
             {value: 'solid'},
             {value: 'great'},
             {value: 'good'},
             {value: 'unproven'}
           ],
           listId: 'test',
           required: true,
           disabled: true,
           order: 3
         }),
   
         new TextboxQuestion({
           value: 'test',
           key: 'firstName',
           label: 'First name',
           placeholder: 'Bombasto',
           required: true,
           visible: true,
           order: 4,
           events: [
             {
               name: 'change',
               actions: [
                 {
                   next: 16,
                   condition: '=',
                   object: 'ABC'
                 }
               ]
             }
           ],
           pattern: '[A-Z](.*)',
           minLength: 1,
           maxLength: 10
         }),
   
         new TextboxQuestion({
           key: 'emailAddress',
           label: 'Email',
           required: true,
           type: 'email',
           order: 5,
           events: [
             {
               name: 'change',
               actions: [
                 {
                   next: 14,
                   condition: '!=',
                   object: 'yami@mail.com'
                 }
               ]
             }
           ],
         }),
   
         new DateQuestion({
           value: '2010-10-12',
           key: 'date',
           label: 'Fecha',
           required: true,
           type: 'date',
           order: 6,
           min: '2000-01-06',
           max: '2030-01-01',
           autocomplete: true,
           step: 6
         }),
   
         new DateQuestion({
           value: '2010-10',
           key: 'month',
           label: 'Mes',
           required: true,
           type: 'month',
           order: 7,
           min: '2000-01',
           max: '2030-01',
         }),
   
         new DateQuestion({
           value: '2010-W06',
           key: 'week',
           label: 'Semana',
           required: true,
           type: 'week',
           order: 8,
           min: '2000-W01',
           max: '2030-W01',
         }),
   
         new DateQuestion({
           value: '2018-06-12T19:30',
           key: 'datetime-local',
           label: 'Datetime local',
           required: true,
           type: 'datetime-local',
           order: 9,
           min: '2017-06-07T00:00',
           max: '2020-06-14T00:00',
         }),
   
          new DateQuestion({
           value: '19:30',
           key: 'time',
           label: 'Hora',
           required: true,
           type: 'time',
           order: 10
         }),
   
         new RadioButtonQuestion({
           key: 'brave',
           label: 'Bravery Rating',
           required: true,
           options: [
             {key: 'solid',  value: 'Solid', disabled: true},
             {key: 'great',  value: 'Great'},
             {key: 'good',   value: 'Good'},
             {key: 'unproven', value: 'Unproven'}
           ],
           disabled: false,
           order: 11
         }),
   
         new CheckboxQuestion({
           key: 'checkbox',
           label: 'Checkbox',
           required: true,
           order: 12,
           checked: true,
           indeterminate: false
         }),
   
         new NumberQuestion({
           key: 'number',
           label: 'Números',
           required: true,
           order: 13,
           min: 6,
           max: 100,
           step: 0.5,
           autocomplete: true,
          // pattern: '[1-9][0-9]*',
         }),
   
         new NumberQuestion({
           key: 'patternNumber',
           label: 'Números con regex',
           required: true,
           order: 14,
           pattern: '[1-9][0-9]*',
           events: [
             {
               name: 'change',
               actions: [
                 {
                   next: 15,
                   condition: '!=',
                   range: [30,50]
                 }
               ]
             }
           ]
         }),
   
         new HtmlQuestion({
           key: 'html',
           order: 15,
           content: "<h1>My First Heading</h1><p>My first paragraph.</p><hr>"
         }),
   
         new DropdownQuestion({
           key: 'brave16',
           label: 'Bravery Rating 16',
           options: [
             {label: 'Seleccione una opcion', value: '', disabled: true},
             {label: 'Solid', value: 'solid'},
             {label: 'Great', value: 'great'},
             {label: 'Good', value: 'good'},
             {label: 'Unproven', value: 'unproven'}
           ],
           multiple: true,
           bindValue: 'value',
           bindLabel: 'label',
           searchable: true,
           clearSearchOnAdd: true,
           closeOnSelect: true,
           required: true,
           visible: false,
           order: 16,
           events: [
             {
               name: 'change',
               actions: [
                 {
                   next: 7,
                   condition: '!=',
                   set: ['great', 'solid']
                 }
               ]
             }
           ]
         })*/
    ];

    return questions.sort((a, b) => a.order - b.order);
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/