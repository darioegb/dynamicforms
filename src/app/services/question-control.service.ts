import { Injectable }   from '@angular/core';
import { FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { QuestionBase, TextboxQuestion, NumberQuestion } from '../models/dynamic';

@Injectable()
export class QuestionControlService {
  constructor() { }

  toFormGroup(questions: QuestionBase<any>[] ) {
    let group: any = {};

    questions.forEach(question => {
      let validators = [];
      if (question.required) {
        validators.push(Validators.required);
      }
     
      if (question instanceof TextboxQuestion) {
        validators = this.textboxValidators(question, validators);
      }

      if (question instanceof NumberQuestion) {
        validators = this.numberValidators(question, validators);
      }
      
      group[question.key] = validators.length ? new FormControl({value:question.value, disabled:question.disabled || ''}, validators)
                                              : new FormControl({value:question.value, disabled:question.disabled || ''} || '');
    });
    return new FormGroup(group);
  }


  textboxValidators(question, validators) {
    if (question.pattern) {
      validators.push(Validators.pattern(question.pattern));
    }
    if (question.minLength) {
      validators.push(Validators.minLength(question.minLength));
    }

    if (question.maxLength) {
      validators.push(Validators.maxLength(question.maxLength));
    }

    if (question.type === 'email') {
      validators.push(Validators.email);
    }
    return validators;
  }

  numberValidators(question, validators) {
    if (question.min) {
      validators.push(Validators.min(question.min));
    }
    if (question.max) {
      validators.push(Validators.max(question.max));
    }
    if (question.pattern) {
      validators.push(Validators.pattern(question.pattern));
    }
    if (question.step) {
      validators.push(this.validateStep(question.step, question))
    }
    return validators;
  }

  validateStep(step: number, question) {
    return (control: AbstractControl): { [key: string]: any } => {
      const value = question.min? control.value - question.min : control.value;     
      if ((value % step) !== 0) {
        return {step: {actualValue: value, requiredStep: step}};
      }
      return null;
    };
  }
}