export const regularExp = {
    containsNumber : /\d+/,
    containsSmallLetters : /[a-z]/,
    containsCapitalLetters : /[A-Z]/
}