import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, ValidationErrors }        from '@angular/forms';

import { QuestionBase }     from '../../models/dynamic/question-base';
import { TranslateService } from '@ngx-translate/core';
import { isBoolean } from 'util';

import { regularExp } from 'src/app/constants/constants';
@Component({
  selector: 'app-question',
  templateUrl: './dynamic-form-question.component.html'
})
export class DynamicFormQuestionComponent {
  @Input() question: QuestionBase<any>;
  @Input() form: FormGroup;
  @Output() changedQuestion = new EventEmitter<QuestionBase<any>>();

  constructor(private translate: TranslateService) {}
  get isValid() { return this.form.controls[this.question.key].valid; }
  getErrorMessage(): string {
    const controlErrors: ValidationErrors = this.form.controls[this.question.key].errors;
    let errorMessage: string;
    if (controlErrors != null) {
          const lastError = Object.entries(controlErrors).pop();
            if (!isBoolean(lastError[1])) {
              errorMessage = this.setErrorMessage(lastError[0], lastError[1]);
            } else {
              errorMessage = this.setErrorMessage(lastError[0]);
            }
          return errorMessage; 
        }
  }
  setErrorMessage(key: string, value?: any): string {
    let errorMessage: string;
    let messageKey = 'validationErrorsMessage.' + key;
    let regex = this.getPropertyByRegex(value, "^((?!actual).)*$");
    if (key === "pattern") {
      errorMessage = this.patternMatchExpression(messageKey, regex);
    } else {
      this.translate.get(messageKey, {param: regex})
      .subscribe((res: string) => errorMessage = res);
    }
     return errorMessage; 
  }
  getPropertyByRegex(obj, regex) {
    let re = new RegExp(regex), key: string;
    for (key in obj) {
      if (re.test(key)) {
        return obj[key];
      }
    }
    return null; // put your default "not found" return value here
 }

 patternMatchExpression(messageKey: string, str: string) {
  let expMatch = {
    containsNumber: regularExp.containsNumber.test(str),
    smallLetters: regularExp.containsSmallLetters.test(str),
    capitalLetters: regularExp.containsCapitalLetters.test(str)
  };
  let patternKey = messageKey + ".";
  let errorMessage = "";

  if (expMatch.smallLetters && expMatch.capitalLetters && expMatch.containsNumber) {
    this.translate.get(patternKey + "alphaNumeric")
    .subscribe((res: string) => errorMessage += res);
  } else if (expMatch.smallLetters && expMatch.capitalLetters) {
    this.translate.get(patternKey + "alphabet")
    .subscribe((res: string) => errorMessage += res);
  } else {
    if(expMatch.smallLetters) {
      this.translate.get(patternKey + "smallLetters")
      .subscribe((res: string) => errorMessage += res);
    } 
    if(expMatch.capitalLetters) {
      this.translate.get(patternKey + "capitalLetters")
      .subscribe((res: string) => errorMessage += res);
    }
     if (expMatch.containsNumber) {
      this.translate.get(patternKey + "numeric")
      .subscribe((res: string) => errorMessage += res);
    } 
  }

  return errorMessage;
}

  displayChildrens(event) {
    if (event.target) {
      this.question.hasCurrentValue = event.target.value ? true : false;
    } else {
      // dropdowns
      if (event instanceof Array) {
        this.question.hasCurrentValue = event.length > 0;
      } else {
        this.question.hasCurrentValue = event.value ? true : false;
      }
    }
    this.changedQuestion.emit(this.question);
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/