import { Component, Input, OnInit }  from '@angular/core';
import { FormGroup }                 from '@angular/forms';
import { QuestionBase }              from '../models/dynamic';
import { QuestionControlService }    from '../services/question-control.service';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  providers: [ QuestionControlService ]
})
export class DynamicFormComponent implements OnInit {

  @Input() questions: QuestionBase<any>[] = [];
  form: FormGroup;
  payLoad = '';

  constructor(private qcs: QuestionControlService) {  }

  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.questions);
    const questionsWithEvents = this.questions.filter(question => question.events);
    if (questionsWithEvents) {
      questionsWithEvents.forEach(question => {
        this.bindEvent(question);
      });
    }
  }

  displayChildrens(question) {
    question.childrens.forEach(childId => {
      const child = this.questions.find(child => child.id === childId);
      child.visible = question.hasCurrentValue;
      // child.disabled = !question.hasCurrentValue;
    });
  }

  bindEvent(question: QuestionBase<any>) {
    question.events.forEach(event => {
      if (event.name === 'change') {
        this.form.controls[question.key].valueChanges.subscribe(value => {
          event.actions.forEach(action => {
            if (action.condition && action.object) {
              this.compareAndJump(action, value);
            } else if (action.range) {
              this.inRange(action, value);
            } else if (action.set) {
              this.valueInSet(action, value);
            }
          });
        });
      }
    });
  }

  compareAndJump(action, value) {
    let jump = false;
    if (value instanceof Array) {
      jump = this.inArray(action.object, value);
      if (action.condition === '!=') {
        jump = !jump;
      }
    } else {
      jump = this.compareWith(action,value);
    }
    this.displayNext(action, jump);
  }

  inArray(object: any, arr: Array<any>) {
    return arr.includes(object);
  }

  hasSubSet(values: Array<any>, arr: Array<any>) {
    return arr.every(val => values.includes(val));
  }

  valueInSet(action, value) {
    const multipleValues = value instanceof Array;
    let jump = multipleValues ? this.hasSubSet(value, action.set) : this.inArray(value, action.set);
    if ( multipleValues && action.condition === '=') {
      jump = jump && this.hasSubSet(action.set, value);
    }

    if (action.condition === '!=') {
      jump = !jump;
    }
    this.displayNext(action, jump);
  }

  compareWith(action, value: any) {
    switch(action.condition) {
      case '>': return value > action.object;
      case '<': return value < action.object;
      case '>=': return value >= action.object;
      case '<=': return value <= action.object;
      case '!=': return value !== action.object;
      default: return value === action.object;
    }
  }

  inRange(action, value) {
    const range = action.range;
    let jump = range[0] <= value && value <= range[1];
    if (action.condition === '!=') {
      jump = !jump;
    }
    this.displayNext(action, jump);
  }

  displayNext(action, jump) {
    const questionIndex = this.questions.findIndex(q => q.id === action.next);
    if (questionIndex) {
      this.questions[questionIndex].visible = jump;
    }
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.form.value);
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/